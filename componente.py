H = [
    [1, 2, 3],
    [2, 3, 6, 8],
    [3, 4, 8, 2],
    [4, 3]
]

G = [
    [1, 2, 3, 4, 5],
    [2, 3, 6, 8, 10, 15],
    [3, 4, 8, 2, 5, 0, 1]
]


def verifyAresta(graph):
    arestas = []
    for i in graph:
        for x in i[1:]:
            arestas.append((i[0], x))
    return arestas

def verifyComponent():
    for i in verifyAresta(H):
        if (i[0], i[1]) not in verifyAresta(G):
            if (i[1], i[0]) not in verifyAresta(G):
                return False
    return True

print(verifyComponent())