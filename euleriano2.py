grafo_arestas = [
    (1, 2), 
    (1, 3), 
    (2, 3), 
    (2, 5), 
    (2, 6), 
    (3, 4), 
    (3, 5), 
    (4, 1)
]

def caminho_euleriano(grafo):
    caminho = []
    caminho.append(grafo[0][0])

    while len(grafo):
        vertice_atual = caminho[len(caminho) - 1]
        for aresta in grafo:
            if vertice_atual in aresta:
                if aresta[0] == vertice_atual:
                    vertice_atual = aresta[1]
                elif aresta[1] == vertice_atual:
                    vertice_atual = aresta[0]
                else:
                    return False

                grafo.remove(aresta)
                caminho.append(vertice_atual)
                print(caminho)
                break
    return caminho




print(caminho_euleriano(grafo_arestas))
