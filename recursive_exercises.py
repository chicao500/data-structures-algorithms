def rep(num):
    if isinstance(num, int):
        if len(str(num)) == 1:
            return num
        else:
            return rep(num//10)
    else:    
        if len('{:.0f}'.format(num)) == 1:
            return int(num)
        else:
            return rep(num/10)

def limpaLista(lista, n):
    if n not in lista:
        return lista
    else:
        lista.remove(n)
        return limpaLista(lista, n)

def perfecto(num, x):
    if x == 1:
        return 1
    
    if num % x == 0:
        return x + perfecto(num, x-1)
    else:
        return perfecto(num, x-1)

    
        
def is_perfecto(num):
    if perfecto(num, num/2) == num:
        return True
    else:
        return False

def primo(num, x=2):
    if num/2 < x:
        return True
    else:
        if num % x != 0:
            return primo(num, x + 1)
        else:
            return False

def listPrimos(num, aux=2, cont=0):
    if num == aux and primo(aux):
        return cont + 1
    elif num == aux:
        return cont
    else:
        if primo(aux):
            return listPrimos(num, aux+1, cont+1)
        else:
            return listPrimos(num, aux+1, cont)



# print(limpaLista([1,2,3,4,5,1,1,1,54,9], 1))
# print(rep(000000000.123123))
# print(is_perfecto(28))

print(listPrimos(13))