def caminho_euleriano(graph):
    caminho = []

    vertice_inicial = graph[0][0]
    caminho.append(vertice_inicial)

    while len(graph):
        vertice_atual = caminho[len(caminho) - 1]
        for aresta in graph:
            if vertice_atual in aresta:
                if aresta[0] == vertice_atual:
                    vertice_atual = aresta[1]
                elif aresta[1] == vertice_atual:
                    vertice_atual = aresta[0]
                else:
                    return False

                graph.remove(aresta)
                caminho.append(vertice_atual)
                break
    return caminho

graph = [(1, 2), (2, 3), (3, 1)]

print (caminho_euleriano(graph))