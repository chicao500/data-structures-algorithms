grafo = [
    [[0, None], [1, None], [3, None]],
    [[1, None], [3, None], [4, None]],
    [[2, None], [0, None], [5, None]],
    [[3, None], [2, None], [4, None], [5, None], [6, None]],
    [[4, None], [6, None]],
    [[5, None]],
    [[6, None], [5, None]]
]

def adjacentes_vertice(v, lista):
    adj = []
    for i in range(1, len(lista[v])):
        adj.append(lista[v][i])
    return adj

def minimo(grafo):
    grafo[2][0][1] = 0
    
    no = grafo[2][0]
    fila = [no]
    visitados = [no[0]]

    distancias = [no]

    while len(fila):
        v = fila.pop(0)

        for vizinho in adjacentes_vertice(v[0], grafo):
            if vizinho[0] not in visitados:
                if vizinho[1] is None:
                    vizinho[1] = v[1] + 1
                visitados.append(vizinho[0])
                fila.append(vizinho)
                distancias.append(vizinho)
                
    print('visitados',visitados)
    print('distancias', distancias)

minimo(grafo)
# print(adjacentes_vertice([3, None], grafo))