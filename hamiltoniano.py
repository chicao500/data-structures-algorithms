def caminho_hamilton(grafo, vertice_ini, caminho=[]):
    if vertice_ini not in set(caminho):
        caminho.append(vertice_ini)
        if len(caminho) == len(grafo):
            return caminho
        for vertice_prox in grafo.get(vertice_ini, []):
            res_caminho = [element for element in caminho]
            candidate = caminho_hamilton(grafo, vertice_prox, res_caminho)
            if candidate is not None:
                return candidate
    else:
        print('caminho percorrido: {}'.format(caminho))


grafo = {1: [2, 4, 5], 2: [1, 3, 5], 3: [2, 4], 4: [1, 3, 5], 5: [1, 2, 4]}
print(caminho_hamilton(grafo, 1))
