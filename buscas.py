
myGrafo = [
    [0, 1, 2],
    [1, 0, 3, 4],
    [2, 0, 5, 6],
    [3, 1],
    [4, 1],
    [5, 2],
    [6, 2]
]

def adjacentes_vertice(v, lista):
    adj = []
    for i in range(len(lista)):
        if (lista[i][0] == v):
            adj = lista[i][1:]
    return adj
print(adjacentes_vertice(0, myGrafo))

def bfs(grafo, no):
    fila = [no]
    visitados = [no]
    
    while len(fila):
        v = fila.pop(0)

        for vizinho in adjacentes_vertice(v, grafo):
            if vizinho not in visitados and vizinho not in fila:
                visitados.append(vizinho)
                fila.append(vizinho)
    return visitados


def dfs(grafo, no):
    visitados = [no]
    pilha = [no]

    while len(pilha):

        v = pilha.pop()
    
        for m in adjacentes_vertice(v, grafo):
            if (m not in visitados):
                visitados.append(m)
                pilha.append(m)
                pilha.append(v)
    return visitados

def dfsRecursive(graph, vertice, visitados=[]):
    if vertice in visitados:
        return visitados 
    visitados.append(vertice)

    for m in adjacentes_vertice(vertice, graph):
        if (m not in visitados):
            visitados = dfsRecursive(graph, m, visitados)
    return visitados

print(bfs(myGrafo, 0))
print(dfs(myGrafo, 0))
print(dfsRecursive(myGrafo, 0))