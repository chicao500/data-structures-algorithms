def criaGrafo(num):
    return [{cont:[]} for cont in range(num)]

def exibirGrafo(grafo):
    print (grafo, '\n\n')

def inserirAresta(grafo, index, value):
    try:
        grafo[index][index].append(value)
        if index is not value:
            grafo[value][value].append(index)
    except:
        print('\nVertice alocado inválido!')
    return grafo

def verifyGrau(grafo, index):
    tamanho = len(grafo[index][index])

    for i in grafo[index][index]:
        if i == index:
            tamanho += 1
    return tamanho

def verifyAdjacencia(grafo, v1, v2):
    if v2 in grafo[v1][v1] or v1 in grafo[v2][v2]:
        return True
    return False

def listAdjacentes(grafo, v1):
    print(grafo[v1][v1])

myGrafo = None

while True:
    print ('\n\n')
    if myGrafo is not None:
        exibirGrafo(myGrafo)
    else:
        print ('Inicialize o grafo antes de tudo!\n')

    print (15*'='+' Escolha uma opção ' + 15*'=')
    print ('1 - Criar um grafo')
    print ('2 - Inserir aresta ')
    print ('3 - Verifique o grau de um vertice')
    print ('4 - Verifique vertices adjacentes')
    print ('5 - Listar todos os vertices adjacentes de um vertice')
    print ('6 - Sair')
    op = int(input('\nEscolha uma opção: '))

    if (op == 1):
        qtdVertice = int(input('Escolha o numero de vértices: '))

        myGrafo = criaGrafo(qtdVertice)

    elif (op == 2):
        aresta = int(input('Digite o vertice para inserir a aresta: '))
        valor = int(input('Digite o valor do proximo vertice para inserir a aresta: '))
        if myGrafo is not None:
            myGrafo = inserirAresta(myGrafo, aresta, valor)

    elif (op == 3):
        vertice = int(input('Informe o vértice: '))
        grauVertice = verifyGrau(myGrafo, vertice)

        print('\n\nGrau do vertice '+str(vertice)+ ' é igual a '+ str(grauVertice))

    elif (op == 4):
        v1, v2 = input('Digite o v1 e v2: ').split(" ")
        if verifyAdjacencia(myGrafo, int(v1), int(v2)):
            print ('O vertice '+v1+' é adjacente ao vertice '+v2+ ' ou vice versa')
        else:
            print ('O vertice '+v1+' não é adjacente ao vertice '+v2+ ' ou vice versa')
    elif (op == 5):
        vertice = int(input('Informe o vertice: '))

        listAdjacentes(myGrafo, vertice)

    elif (op == 6):
        break
    
    else:
        print ('\nOpção inválida!\n')
